import toxi.geom.*;
import toxi.geom.mesh.*;
import toxi.processing.*;
import nervoussystem.obj.*;

PImage img;

//radius of pixel brightness avg, higher value means more blurred edges aka higher resolution
int radius = 12;
//radius of cylinder, smaller value means more detail
int radiusCylinder = 2;
//space between cylinders
int spacing = 8;
//number of cylinder sides, higher means sharper individual cylinders
int sides = 50;
//changes the height of the individual cylinders
float factor = 0.2;
//Creates an initial extrusion of 
float initialExtrusion = 20;

//inverts the color of the image
boolean invert = false;

//Height of the rectangular base
int baseHeight = 5;

boolean record = false;

void setup() {
  clear();
  size(600, 600, P3D);
  // Make a new instance of a PImage by loading an image file
  img = loadImage("rsz_opera_logo-05.png");
  img.loadPixels();
  for(int j=0; j < img.height; j++){
    for(int i=0; i < img.width; i++){
      float g = gammaHeight(i, j);
      img.pixels[j*img.width+i] =  color(g, g, g);
    }
  }
  if(invert){
    img.filter(INVERT);
  }
  println("done");
  img.updatePixels();
}

void draw() {
  if(record){
    beginRecord("nervoussystem.obj.OBJExport", "try15.obj");
  }
  background(205);
  // Draw the image to the screen at coordinate (0,0)
  //image(img,0,0);
  drawBox();
  cylinderMatrix();
  if(record){
    endRecord();
    record = false;
  }
  println("drawn");
}

void cylinderMatrix(){
  for(int y=radiusCylinder*2; y < img.height-radiusCylinder; y+=2*radiusCylinder+spacing){
    pushMatrix();
    translate(0, y);
    for(int x=radiusCylinder*2; x < img.width-radiusCylinder; x+=2*radiusCylinder+spacing){
      pushMatrix();
      if(record)
        translate(img.width-x, 0);      
      else
        translate(x, 0);
      drawCylinder(sides, radiusCylinder, brightness(img.get(x, y))*factor+initialExtrusion);
      popMatrix();
    }
    popMatrix();
  }
}


//draws the rectangular base
void drawBox(){
  pushMatrix();
  translate(img.width/2, img.height/2, baseHeight/2);
  box(img.width, img.height, baseHeight);
  popMatrix();
}

//draws a cylinder
void drawCylinder( int sides, float r, float h)
{
    float angle = 360 / sides;
    float halfHeight = h / 2;
    pushMatrix();
    translate(0, 0, halfHeight);
    // draw top of the tube
    beginShape();
    for (int i = 0; i < sides; i++) {
        float x = cos( radians( i * angle ) ) * r;
        float y = sin( radians( i * angle ) ) * r;
        vertex( x, y, -halfHeight);
    }
    endShape(CLOSE);
 
    // draw bottom of the tube
    beginShape();
    for (int i = 0; i < sides; i++) {
        float x = cos( radians( i * angle ) ) * r;
        float y = sin( radians( i * angle ) ) * r;
        vertex( x, y, halfHeight);
    }
    endShape(CLOSE);
 
    // draw sides
    beginShape(TRIANGLE_STRIP);
    for (int i = 0; i < sides + 1; i++) {
        float x = cos( radians( i * angle ) ) * r;
        float y = sin( radians( i * angle ) ) * r;
        vertex( x, y, halfHeight);
        vertex( x, y, -halfHeight);    
    }
    endShape(CLOSE);
    popMatrix();
}


float gammaHeight(int x, int y){
  return getGamma(x, y)/(3.1415*sq(radius));
}


//calculates the brightness value of a pixel
float getGamma(int x, int y){
   int startX = max(x-radius, 0);
   int startY = max(y-radius, 0);
   float gammaSum = 0;
   
   for(int j=startY; j < startY + 2*radius; j++){
     for(int i=startX; i < startX + 2*radius; i++){
       if(i >= img.width || j >= img.height){
         break;
       }
       // The functions red(), green(), and blue() pull out the 3 color components from a pixel.
       float r = red(img.pixels[loc(i, j)]);
       float g = green(img.pixels[loc(i, j)]);
       float b = blue(img.pixels[loc(i, j)]);
       if(dist(i, j, x, y) <= radius){
         gammaSum +=  getGrayScale(r, g, b);
       }
     }
   }
  
  return gammaSum;
}

//calculates location of pixel in the array
int loc(int row, int col){
  return col*img.width + row;
}

//calculates distance between 2 points
float dist(int x1, int y1, int x, int y){
  return sqrt(sq(x1-x) + sq(y1-y));
}

//calculates weighted grayscale value
float getGrayScale(float r, float g, float b){
  return (0.3*r + 0.59*g + 0.11*b);
}

void keyPressed(){
  println("record");
  if(key == 's'){
    record = true;
  }
}
